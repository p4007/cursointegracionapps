# Curso - Integración de Aplicaciones con Camel

> Ejemplo de dos proyectos utilizando el framework de [Apache Camel](https://camel.apache.org/ "Apache Camel"),

> - El primero de ellos IntegratorJava implementa una configuración basica para la ejecución de ciertas rutas simples con el fin de ejemplificar su uso y ventajas.

> - El segundo IntegratorSpring es un ejemplo mas complejo utilizando otros componentes mas ccomplejos.


------------
####  Pasos para generar los proyectos
>  Les comparto este comando que con ayuda de Maven  pueden generar el proyecto en base al arquetipo.

```
 mvn archetype:generate -B   \
 -DarchetypeGroupId=org.apache.camel.archetypes \
 -DarchetypeArtifactId=camel-archetype-java \
 -DarchetypeVersion=3.13.0 \
 -DgroupId=com.curso.integracion.aplicaciones \
 -DartifactId=integradorJavaSimple   \
 -Dversion=0.0.1-SNAPSHOT
```
>   Donde...
> - **-B** es una flag para no entrar en el modo interactivo.
> - **DarchetypeGroupId** =  &lt;archetype-groupId &gt; 
> - **DarchetypeArtifactId**=&lt;archetype-artifactId&gt;
> - **DarchetypeVersion**=&lt;version del arquetipo a utilizar&gt;
> - **DgroupId**=&lt; group Id del proyecto &gt;
> - **DartifactId**=&lt; artifact Id del proyecto &gt;
> - **Dversion**=&lt; version del proyecto por lo general **0.0.1-SNAPSHOT** &gt;
