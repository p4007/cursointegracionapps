package com.curso.integracion.aplicaciones.apache.camel.entity;

import lombok.Data;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/*clase Employees para ejemplificar el uso de la funcionalidad unmarshal*/
@CsvRecord(separator = ",", skipFirstLine = true )
@Data
public class Employees {

  @DataField(pos = 1)
  private String rfc;
  
  @DataField(pos = 2)
  private int employeeId;
  
  @DataField(pos = 3)
  private String name;
  
  @DataField(pos = 4)
  private String lastName;
  
  @DataField(pos = 5)
  private String area;
  
  
  
}
