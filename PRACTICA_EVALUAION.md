#Integración de Aplicaciones con Apache Camel
## Práctica de Evaluación (Carga de Opciones a Meses en una Tienda Departamental)

----------
El  ejercicio constara en tomar la información sobre Opciones de pago para una tienda departamental. Dicha información se debe tomar del archivo prefijosMeses.csv que se encuentra en este repo; el archivo tiene depositarse en la siguiente ruta: **${HOME}/Integrator/InputFiles**, dicha carga del archivo no debe ser directa si no a travez de un API ya sea expuesta con Spring o Camel, para despues almacenar en base de datos los datos obtenidos.

La API debe exponer los siguientes endpoints
> POST:  /opcionesPago/            --ruta para crear nuevas opciones de pago
> POST:  /opcionesPago/cargaArchivo   --ruta para cargar archivo
> GET:   /opcionesPago/  -- ruta para cconsultar todas las opciones de pago
> GET:   /opcionesPago/archivos  -- ruta para consultar los archicos cargados
>GET:    /opcionesPago/{id}  --ruta para consultar las opciones de pago por Id
>PUT: /opcionesPago  --ruta para actualizar las opciones de pago
>DELETE: /opcionesPago/{id} --ruta para eliminar las opciones de pago

**RN: cuando la consulta cuando no devuelva elementos deberá de iniciar la carga del archivo y mostar resultados. **

####*Estructura de  base de datos*

la informacion obtenida se debe almacenar en la tabla de opcionesMeses y archivosProcesados, se comparte la siguiente estructura:

##### Tabla opcionesMeses:
> opcionId int
prefijoTarjeta int
institutcionBancaria varchar( 20)
tipoTarjeta varchar (1)
nombreBanco varchar (50)
estatus int(1)
tipoPago  varchar (1)
monto3Meses int  
monto6Meses int
monto12Meses int
creadoEn timestamp
cargaArchivo int -- 0 para garga por api, 1 para carga de archivo
exchangeId varchar(100)

##### Tabla archivosProcesados:

idArchivo int
nombre varchar(100)
createdAt timestamp
registrosProcesados int
exchangeId varchar(100)


## Consideraciones

El proyecto debe ser generado por el arquetipo.
El proceso debera tener el unmarshall.
El proyecto no debera tener errores o problemas para compilar.
Se debera usar un archivo de properties.
Se tiene que configurar un componente extra y hacer una prueba sencilla del mismo, ejemplo: 
**JMS**: enviar un mensaje sencillo
**QUARTZ**: configurar un cron
**SMTP**: enviar un correo de ejemplo
**ACTIVEMQ**:recibir un mensaje o enviarlo



